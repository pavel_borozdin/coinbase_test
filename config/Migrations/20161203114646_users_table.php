<?php

use Phinx\Migration\AbstractMigration;

class UsersTable extends AbstractMigration {

	public function change() {
		$table = $this->table('users');
		$table->addColumn('user_id', 'string', [
			'null' => false,
		]);
		$table->addColumn('coinbase_id', 'string', [
			'default' => null,
			'null' => true,
		]);
		$table->addColumn('user_name', 'string', [
			'limit' => 255,
			'null' => false,
		]);
		$table->addColumn('access_token', 'text', [
			'default' => null,
			'null' => true,
		]);
		$table->addColumn('refresh_token', 'text', [
			'default' => null,
			'null' => true,
		]);
		$table->addColumn('expires_at', 'datetime', [
			'default' => null,
			'null' => true,
		]);
		$table->addColumn('state', 'string', [
			'limit' => 255,
			'null' => false,
		]);
		$table->addColumn('created', 'timestamp', [
			'default' => null,
			'null' => true,
		]);
		$table->addColumn('modified', 'timestamp', [
			'default' => null,
			'null' => true,
		]);
		$table->create();
	}

}
