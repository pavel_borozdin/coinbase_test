<?php

namespace App\Controller;

use Cake\Http\Client as CakeHttpClient;
use Cake\I18n\Time;
use Cake\Event\Event;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Resource\Transaction;
use Coinbase\Wallet\Value\Money;
use Coinbase\Wallet\Resource\Account;

class ApiController extends AppController {

	public $scopes = [
		'wallet:accounts:read',
		'wallet:addresses:read',
		'wallet:addresses:create',
		'wallet:transactions:read',
		'wallet:transactions:request',
	];

	public function initialize() {
		parent::initialize();
		$this->loadComponent('RequestHandler');
		$this->viewBuilder()->layout(false);
		$this->loadModel('Users');

		if ($this->request->action != 'info') {
			$this->RequestHandler->renderAs($this, 'json');
			$this->response->type('application/json');
			$this->set('_serialize', true);
		}
	}

	public function beforeFilter(Event $event) {
		parent::beforeFilter($event);
		if ($this->request->is('post')) {
			if ($this->request->data('command') != '/init') {
				$user = $this->Users->find()->where(['user_id' => $this->request->data('user_id')])->firstOrFail();
				if ($user->expires_at <= Time::now()) {
					if (is_scalar($user = $this->refresh($user))) {
						$this->set('message', $user);
						return $this->render('index');
					}
				}
				$configuration = Configuration::oauth($user->access_token, $user->refresh_token);
				$this->client = Client::create($configuration);
			}
		}
	}

	public function init() {
		if ($this->request->is('post') && $this->request->data('token') == INIT_TOKEN) {
			if (!($user = $this->Users->find()->where(['user_id' => $this->request->data('user_id')])->first())) {
				$user = $this->Users->newEntity($this->request->data);
			}
			$user->state = md5(time() . rand(0, 10000));
			$this->Users->save($user);
			$url = 'https://www.coinbase.com/oauth/authorize?';
			$redirectUrl = 'http://slack.bezuma.net/api/register';
			$message = $url . 'response_type=code&client_id=' . COINBASE_CLIENT_ID . '&redirect_uri=' . $redirectUrl . '&state=' . $user->state . '&scope=' . implode(',', $this->scopes);
			$this->set('text', $message);
		}
	}

	public function wallet() {
		if ($this->request->is('post') && $this->request->data('token') == WALLET_TOKEN) {
			switch ($this->request->data('text')) {
				case 'balance':
					$account = $this->client->getPrimaryAccount();
					$message = 'Your balance is: ' . $account->getBalance()->getAmount() . ' ' . $account->getBalance()->getCurrency();
					break;
			}
			$this->set('text', $message);
		}
		return $this->render('index');
	}

	/*
	 * @params
	 * {0} - command
	 * {1} - username
	 * {2} - description
	 * {3} - amount in BTC
	 */

	public function btc() {
		if ($this->request->is('post') && $this->request->data('token') == BTC_TOKEN) {

			$user = $this->Users->find()->where(['user_id' => $this->request->data('user_id')])->first();
			$configuration = Configuration::oauth($user->access_token, $user->refresh_token);
			$client = Client::create($configuration);
			try {
				foreach ($client->getAccounts() as $toAccount) {
					if ($toAccount->getCurrency() == 'BTC') {
						break;
					}
				}

				$string = explode(' ', $this->request->data('text'));
				$user2 = $this->Users->find()->where(['user_name' => $string[1]])->firstOrFail();
				$configuration2 = Configuration::oauth($user2->access_token, $user2->refresh_token);
				$client2 = Client::create($configuration2);

				foreach ($client2->getAccounts() as $fromAccount) {
					if ($fromAccount->getCurrency() == 'BTC') {
						break;
					}
				}

				$account = Account::reference($toAccount->getId());
				$transaction = Transaction::request([
					'to' => $account,
					'toEmail' => 'paul.borozdin@gmail.com',
					'amount' => new Money($string[3], CurrencyCode::BTC),
					'description' => $string[2],
				]);


				$client->createAccountTransaction($fromAccount, $transaction);
				$address = $transaction->getAddress();
				$label = 'some name' . rand(1, 50);
				$address->setName('some name' . rand(1, 50));
				$message = 'Awaiting' . $string[3] . 'BTC' . 'to' . $transaction->getAddress()->getId();
				header('Content-Type: application/json');
				echo json_encode([
					'text' => $message,
					'attachments' => [
						[
							'image_url' => 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=' . $transaction->getAddress()->getId(),
						],
						[
							'text' => 'bitcoin:<' . $transaction->getAddress()->getId() . '>[?amount=<' . $string[3] . '>][[?|&]label=<' . $label . '>][[?|&]message=<' . $string[2] . '>]'
						],
					]
				]);
				exit;
			} catch (\Coinbase\Wallet\Exception\ValidationException $ex) {
				$message = $ex->getMessage();
				$this->set('text', $message);
			} catch (\Coinbase\Wallet\Exception\ExpiredTokenException $ex) {
				$this->set('text', $ex->getMessage());
			} catch (Coinbase\Wallet\Exception\NotFoundException $ex) {
				$this->set('text', $ex->getMessage());
			}
		}
	}

	public function register($returnUrl = 'http://slack.bezuma.net/api/register') {
		if ($code = $this->request->query('code')) {
			if ($user = $this->Users->find()->where(['state' => $this->request->query('state')])->first()) {
				$http = new CakeHttpClient();
				$response = $http->post('https://api.coinbase.com/oauth/token', [
					'grant_type' => 'authorization_code',
					'code' => $code,
					'client_id' => COINBASE_CLIENT_ID,
					'client_secret' => COINBASE_SECRET_KEY,
					'redirect_uri' => $returnUrl,
				]);
				if (is_scalar($result = $this->Users->updateUserToken($user, $response))) {
					$this->set('message', $result);
				} else {
					$configuration = Configuration::oauth($user->access_token, $user->refresh_token);
					$this->client = Client::create($configuration);
					if (!$user->coinbase_id) {
						$user->coinbase_id = $this->client->getCurrentUser()->getId();
						$this->Users->save($user);
					}
					return $this->redirect(['controller' => 'Api', 'action' => 'info']);
				}
			}
		}
		return $this->render('index');
	}

	public function refresh($user, $returnUrl = 'http://slack.bezuma.net/api/register') {
		$http = new CakeHttpClient();
		$response = $http->post('https://api.coinbase.com/oauth/token', [
			'grant_type' => 'refresh_token',
			'redirect_uri' => $returnUrl,
			'client_id' => COINBASE_CLIENT_ID,
			'client_secret' => COINBASE_SECRET_KEY,
			'refresh_token' => $user->refresh_token,
		]);
		return $this->Users->updateUserToken($user, $response);
	}

	public function info() {}

}
