<?php

namespace App\Model\Table;

use Cake\I18n\Time;

class UsersTable extends AppTable {

	public function initialize(array $config) {
		parent::initialize($config);
		$this->addBehavior('Timestamp');
	}

	public function updateUserToken($user, $response) {
		$json = $response->json;
		if ($response->isOk()) {
			if (isset($json['access_token'])) {
				$this->patchEntity($user, $json);
				$now = Time::now();
				$user->expires_at = $now->addSeconds(7200)->format('Y-m-d H:i:s');
				$user->state = null;
				$this->save($user);
				return $user;
			}
		} else {
			return $json['error_description'];
		}
	}

}
